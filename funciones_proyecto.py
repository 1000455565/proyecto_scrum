
#importar funcion de funciones.py
#from funciones import space
from sql_proyectos import insert_proyectos,select_all_proyectos,consultar_proyecto
from sql_user_story import consultar_userstory
from funciones import validacion_not_null, validacion_null
from tabulate import tabulate

#crear funcion 
def crear_proyecto():
    # asignacion de variables
    nombre = ""
    validacion_nombre = 0
    while validacion_nombre == 0:

        #validacion
        nombre = input("digite el nombre del Proyecto: ")
        validacion_nombre = validacion_not_null(nombre, 200)
        
    
    descripcion = ""
    validacion_descripcion = 0
    while validacion_descripcion == 0:
        
        descripcion = input ("digite la descripcion del proyecto: ")
           #validar que no este vacio
        validacion_descripcion = validacion_null (descripcion, 1000)
    
    
    insert_proyectos(nombre, descripcion)

def consulta_proyecto():

    select_all_proyectos()

def consult_proyecto():
    nombre_proyecto = ""
    validacion_nombre_proyecto = 0
    proyectos = []
    userStories = []
    userStori = []
    headerUser = ['Codigo','Nombre']
    while validacion_nombre_proyecto == 0:
        #validacion
        nombre_proyecto = input("digite el nombre del proyecto: ")
        validacion_nombre_proyecto = validacion_not_null(nombre_proyecto, 200)
        if validacion_nombre_proyecto == 1:
            proyectos = consultar_proyecto('proyectos','nombre',nombre_proyecto)
            if proyectos:
                proyecto = proyectos[0]
                print(f'Proyecto: {proyecto[1]}')
                userStori = consultar_userstory(proyecto[0])
                if userStori:
                    userStories.append(headerUser)
                    for h in userStori: 
                        headerUser =[]                       
                        headerUser = [h[1],h[2]]
                        userStories.append(headerUser)

                    print(tabulate(userStories))
                else:
                    print('No hay Userstory asignadas')


            else:
                print('No se encontro proyecto')
                validacion_nombre_proyecto = 0

   # print(proyecto)    
    
   