# importar una funcion 

from funciones_proyecto import crear_proyecto, consulta_proyecto, consult_proyecto
from funciones_user_story import crear_user_story,consulta_userstory,eleminar_userstory,editar_userstory
#from funciones_proyecto import consulta_proyecto

#inicialización de variable
opcion = 0

# expresion booleana
while opcion != 8:

    #mostrar opciones
    print ("[1] Crear proyecto")
    print ("[2] Consultar lista de proyectos")
    print ("[3] Consultar Proyecto")
    print ("[4] Crear User story")
    print ("[5] Consultar User Story")
    print ("[6] Editar User Story")
    print ("[7] Eliminar User Story")
    print ("[8] salir")


    # se pide dato por consola y se convierte a entero 
    valor_digitado = input ("digite la opcion: ")
    opcion = int(valor_digitado)
    #print(opcion)

    #validacion de opcion 
    if opcion == 1:
        print(" creando proyecto")
        crear_proyecto()
    elif opcion == 2:
        print ("consultando lista de proyectos")
        consulta_proyecto()
    elif opcion == 3:
        print ("Consultar proyecto")
        consult_proyecto()
    elif opcion == 4:
        print ("Creando User Story")
        crear_user_story()
    elif opcion == 5:
        print ("Consultar User Story")
        consulta_userstory()
    elif opcion == 6:
        print ("Editar User Story")
        editar_userstory()
    elif opcion == 7:
        print ("Eliminar User Story")
        eleminar_userstory()
    elif opcion == 8:
        print ("salir")
    
print("acabamos")

