create table proyectos(
id serial primary key not null,
nombre varchar (200) not null,
descripcion varchar (1000)
);

create table UserStories(
id serial primary key not null,
codigo varchar (45) not null unique,
nombre varchar (500) not null,
card varchar (5000),
converstion varchar (5000),
confirmation varchar (5000),
idproyecto serial not null,
foreign key (idproyecto) references proyectos(id)
);
