from funciones import validacion_not_null, validacion_null
from sql_user_story import campo_unico,consultar_proyecto,insert_user_story,consultar_userstory_by_codigo,eliminar_hu,update_userStory
from sql_proyectos import consultar_proyecto_by_id
from tabulate import tabulate
#crear funcion 
def crear_user_story():
    
    codigo = ""
    validacion_codigo = 0
    while validacion_codigo == 0:

        codigo = input("digite el codigo del user story: ")
        validacion_codigo = validacion_not_null(codigo, 45)
        if validacion_codigo == 1:
            validacion_codigo =campo_unico('UserStories','codigo',codigo)
            if validacion_codigo == 0:
                print("el codigo el user story ya se encuentra en la base de datos")
              
    nombre = ""
    validacion_nombre = 0
    while validacion_nombre == 0:

        nombre = input("digite el nombre del user story: ")
        validacion_nombre = validacion_not_null(nombre, 500)

    card = ""
    validacion_card = 0
    while validacion_card == 0:

        card = input("digite el card del user story: ")
        validacion_card = validacion_null (card, 5000)

    converstion = ""
    validacion_converstion = 0
    while validacion_converstion == 0:

        converstion = input("digite el conversation del user story: ")
        validacion_converstion = validacion_null (converstion, 5000)

    confirmation = ""
    validacion_confirmation = 0
    while validacion_confirmation == 0:

        confirmation = input("digite el confirmation del user story: ")
        validacion_confirmation = validacion_null (confirmation, 5000)

    nombre_proyecto = ""
    validacion_nombre_proyecto = 0
    while validacion_nombre_proyecto == 0:

        nombre_proyecto = input("digite el nombre del proyecto para asignar al user story: ")
        validacion_nombre_proyecto = validacion_not_null (nombre_proyecto, 200)
        if validacion_nombre_proyecto == 1:
            validacion_nombre_proyecto =consultar_proyecto('proyectos','nombre',nombre_proyecto)
            print(f'validacion_nombre_proyecto: {validacion_nombre_proyecto}')
            if validacion_nombre_proyecto == 0:
                print("el nombre del Proyecto no se encuentra en la base de datos")

    insert_user_story(codigo, nombre, card, converstion, confirmation, validacion_nombre_proyecto)

def consulta_userstory():
    
    codigo = ""
    validacion_codigo = 0
    lista_userstori = []
    while validacion_codigo == 0:
        codigo = input("digite el codigo del user story: ")
        validacion_codigo = validacion_not_null(codigo, 45)
        if validacion_codigo == 1:
            ustori = consultar_userstory_by_codigo(str(codigo))
            if ustori:
                headeruser = ['Nombre del Proyecto','Código del User Story','Nombre del User Story',
                'Card del User Story','Conversation del User Story','Confirmation del User Story']
                u = ustori[0]
                proyectos = consultar_proyecto_by_id(str(u[6]))
                proyecto = proyectos[0]
                lista_userstori.append(headeruser)
                hu = [str(proyecto[1]),str(u[1]),str(u[2]),str(u[3]),str(u[4]),str(u[5])]
                lista_userstori.append(hu)
                
                print(tabulate(lista_userstori))
            else:
                print('No se encontrarón User Story')

def eleminar_userstory():
    
    codigo = ""
    validacion_codigo = 0
    while validacion_codigo == 0:
        codigo = input("digite el codigo del user story para Eliminar: ")
        validacion_codigo = validacion_not_null(codigo, 45)
        if validacion_codigo == 1:
            validacion_codigo = eliminar_hu(codigo)

def editar_userstory():
    
    codigo = ""
    validacion_codigo = 0
    while validacion_codigo == 0:
        codigo = input("digite el codigo del user story a editar: ")
        validacion_codigo = validacion_not_null(codigo, 45)
        if validacion_codigo == 1:
            userStories = consultar_userstory_by_codigo(codigo)
            if userStories:
                userStory = userStories[0]
                validacion_codigo = opcion_editar(userStory)
            else:
                validacion_codigo = 0
                print('No se encontrarón User Story')

def opcion_editar(userStory):
    opcion = 0
    while opcion == 0:
        print (f"[1] Nombre = {userStory[2]}")
        print (f"[2] Card = {userStory[3]}")
        print (f"[3] Conversation = {userStory[4]}")
        print (f"[4] Confirmation = {userStory[5]}")
        print ("[0] salir")
        valor_digitado = input ("digite la opcion: ")
        opcion = int(valor_digitado)
        #validacion de opcion 
        if opcion == 1:
            print(" Editando Nombre")
            nombre = validarnombre()
            opcion = update_userStory('nombre',nombre,userStory[0])
        elif opcion == 2:
            print(" Editando Card")
            card = validarcard()
            opcion = update_userStory('card',card,userStory[0])
        elif opcion == 3:
            print(" Editando Conversation")
            converstion = validarconverstion()
            opcion = update_userStory('converstion',converstion,userStory[0])
        elif opcion == 4:
            print(" Editando Confirmation")
            confirmation = validarconfirmation()
            opcion = update_userStory('confirmation',confirmation,userStory[0])
    return opcion

def validarnombre():
    nombre = ""
    validacion_nombre = 0
    while validacion_nombre == 0:

        nombre = input("digite el nombre del user story: ")
        validacion_nombre = validacion_not_null(nombre, 500)
    return nombre

def validarcard():
    card = ""
    validacion_card = 0
    while validacion_card == 0:

        card = input("digite el card del user story: ")
        validacion_card = validacion_null (card, 5000)
    return card

def validarconverstion():
    converstion = ""
    validacion_converstion = 0
    while validacion_converstion == 0:

        converstion = input("digite el conversation del user story: ")
        validacion_converstion = validacion_null (converstion, 5000)
    return converstion

def validarconfirmation():
    confirmation = ""
    validacion_confirmation = 0
    while validacion_confirmation == 0:

        confirmation = input("digite el confirmation del user story: ")
        validacion_confirmation = validacion_null (confirmation, 5000)
    return confirmation





