# Importar la libreria de acceso a Postgres
import psycopg2
from datos_conexion import dc
from tabulate import tabulate

# 1. Conexion a la base de datos Postgres
conexion = psycopg2.connect(**dc)

# 2. Crear el cursor que ejecuta las sentencias SQL
cursor = conexion.cursor()

#validar campo unico
def campo_unico(tabla,campo,valor):
    # 3. Establece la sentencia SQL a ejecutar
    sql = f"select count(1) from {tabla} where {campo}='{valor}'"
    # 5. Ejecuta la sentencia SQL
    cursor.execute(sql)
    # 6. validar si hay dato
    if cursor.fetchone()[0]:        
        return 0     
    
    return 1

#validar campo unico
def consultar_proyecto(tabla,campo,valor):
    # 3. Establece la sentencia SQL a ejecutar
    sql = f"select * from {tabla} where {campo}='{valor}'"
    # 5. Ejecuta la sentencia SQL
    cursor.execute(sql)
    # 6. validar si hay dato
    registros = cursor.fetchall() 
    for registro in registros:
        return registro[0]      
    return 0

def insert_user_story(codigo, nombre, card, conversation, confirmation,id_proyecto):
    # 3. Crear la sentencia SQL
    sql = 'insert into UserStories(codigo, nombre, card, converstion, confirmation, idproyecto) values(%s, %s, %s, %s, %s, %s)'
    # 4. Define los parametros
    parametros = (codigo, nombre, card, conversation, confirmation, id_proyecto)
    try:
        # 5. Ejecuta la sentencia
        cursor.execute(sql, parametros)
        # 6. Guarda los datos en la BD
        conexion.commit()
        print('Se guardaron los datos exitosamente!')
    except:
        print('Hubo un error. Intente mas tarde.')

def consultar_userstory(id_proyecto):
    # Crear la sentencia SQL
    sql = 'select * from userstories where idproyecto=%s'
    # Define los parametros
    parametros = (str(id_proyecto))
    # Ejecuta la sentencia SQL
    cursor.execute(sql,parametros)
    # validar si hay dato
    registros = cursor.fetchall() 
    return registros 

def consultar_userstory_by_codigo(cod):
    # Crear la sentencia SQL
    sql = f"select * from userstories where codigo='{cod}'"
    cursor.execute(sql)
    # validar si hay dato
    registros = cursor.fetchall() 
    return registros 

def eliminar_hu(codigo):
    # 3. Crear la sentencia SQL
    sql = f"delete from userstories where codigo='{codigo}'"
   # parametros = (str(codigo))

    try:
        # Ejecuta la sentencia
        cursor.execute(sql)

        # Elimina los datos en la BD
        conexion.commit()
        print('Se eliminó la User Story')
    except:
        print('Hubo un error. Intente mas tarde.')
        return 0
    return 1

def update_userStory(campo, valor, id_u):
    # Crear la sentencia SQL
    sql = f"update userstories set {campo} = '{valor}' where id='{id_u}'"
   # print(f'sql: {sql}')
    # Ejecuta la sentencia
    try:
       cursor.execute(sql)
       # Guarda los datos en la BD
       conexion.commit()
       print('Se guardaron los datos exitosamente!')
    except:
       print('Hubo un error. Intente mas tarde.')
       return 0 
    return 1

