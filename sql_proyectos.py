# Importar la libreria de acceso a Postgres
import psycopg2
from datos_conexion import dc
from tabulate import tabulate

# 1. Conexion a la base de datos Postgres
conexion = psycopg2.connect(**dc)

# 2. Crear el cursor que ejecuta las sentencias SQL
cursor = conexion.cursor()

def insert_proyectos(nombre, descripcion):
    # 3. Crear la sentencia SQL
    sql = 'insert into proyectos(nombre, descripcion) values(%s, %s)'
    # 4. Define los parametros
    parametros = (nombre, descripcion)
    try:
        # 5. Ejecuta la sentencia
        cursor.execute(sql, parametros)
        # 6. Guarda los datos en la BD
        conexion.commit()
        print('Se guardaron los datos exitosamente!')
    except:
        print('Hubo un error. Intente mas tarde.')
    

# crear funcion para consultar
def select_all_proyectos():
    # 3. Ejecuta la sentencia SQL
    cursor.execute('select * from proyectos order by id')

    # 4. Obtiene todos los registros
    registros = cursor.fetchall()

    proyecto = ['Nombre','Descripción']
    proyectos = []
    proyectos.append(proyecto)
    # 5. Recorrer los registros
    for registro in registros:
        proyecto = []
        proyecto = [registro[1], registro[2]]
        proyectos.append(proyecto)
    
    print(tabulate(proyectos))

def consultar_proyecto(tabla,campo,valor):

 # 3. Establece la sentencia SQL a ejecutar
    sql = f"select * from {tabla} where {campo}='{valor}'"
    # 5. Ejecuta la sentencia SQL
    cursor.execute(sql)
    # 6. validar si hay dato
    registros = cursor.fetchall() 
    #for registro in registros:
    return registros

def consultar_proyecto_by_id(id):
    # Establece la sentencia SQL a ejecutar
    sql = f"select * from proyectos where id='{id}'"
    cursor.execute(sql)
    # validar si hay dato
    registros = cursor.fetchall() 
    return registros









